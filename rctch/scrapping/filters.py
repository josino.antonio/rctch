from rctch.scrapping.models import Noticia
import django_filters

class NoticiaFilter(django_filters.FilterSet):

    titulo = django_filters.CharFilter(lookup_expr='icontains')
    
    class Meta:
        model = Noticia
        fields = ['titulo']