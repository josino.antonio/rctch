from django.core.management.base import BaseCommand
from django.utils import timezone

import requests
from bs4 import BeautifulSoup
from rctch.scrapping.models import Noticia


class Command(BaseCommand):
    help = 'Scrapping paginas de noticias'

    def handle(self, *args, **kwargs):

        r = requests.get('https://www.tecmundo.com.br/').text

        soup = BeautifulSoup(r, 'html.parser')
        
        noticias_destaque = soup.findAll("div", {"class": 'nzn-main-text'})

        for noticia_destaque in noticias_destaque:
            nova_noticia = Noticia()
            nova_noticia.titulo = noticia_destaque.find("h2").get_text(strip=True)            
            nova_noticia.save()
