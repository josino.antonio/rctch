# Generated by Django 2.2 on 2019-05-31 02:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scrapping', '0003_auto_20190531_0215'),
    ]

    operations = [
        migrations.RenameField(
            model_name='noticia',
            old_name='scrapping_dates',
            new_name='scrapping_date',
        ),
    ]
