from django.db import models
from django.utils.translation import gettext_lazy as _

class Noticia(models.Model):
    
    titulo = models.TextField('Titulo')    
    class Meta:
        db_table = 'noticias'  
    
    def __str__(self):
        return self.titulo
