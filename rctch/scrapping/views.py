from django.shortcuts import render
from django.http import HttpResponse

from rctch.scrapping.models import Noticia
from .filters import NoticiaFilter

def home(request):
   noticias = Noticia.objects.all()
   template_name = 'index.html'
   context = {      
      'filter' : NoticiaFilter(request.GET, queryset=noticias)
   }
   return render(request, template_name , context)

def search(request):
   
   noticia_list = Noticia.objects.all()
   noticia_filter = NoticiaFilter(request.GET, queryset=noticia_list)
   
   return render(request, 'index.html', {'filter': noticia_filter})
